$(document).ready(function () {
    var neg = $('.main-header').outerHeight() + $('.main-footer').outerHeight();
    var window_height = $(window).height();
    if (window_height >= neg) {
        $(".content-wrapper").css('min-height', window_height - neg);
    }

    $('[data-toggle="offcanvas"]').on('click', function () {
        $('.offcanvas-collapse').toggleClass('open');
        $('.navbar').addClass('fixed-top')
    })

});